﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace WH_TalentSoft.Hubs
{
    public class ChatHub : Hub
    {
        public void Send(string key,string realise,string status)
        {
            //Clients.All.hello();
            Clients.All.addNewMessageToPage(key, realise, status);
        }
    }
}