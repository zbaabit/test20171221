﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WH_TalentSoft.Models;

namespace WH_TalentSoft.ViewModel
{
    public class HoursRealise
    {
        public SOURCE_TALENT source { get; set; }
        public ViewSESSION1 session { get; set; }
        public ViewTRAINEE trainee{ get; set; }

        public string search1 { get; set; }
        public string search2 { get; set; }
        public string search3 { get; set; }
        public string search4 { get; set; }
        public string search5 { get; set; }

    }
}