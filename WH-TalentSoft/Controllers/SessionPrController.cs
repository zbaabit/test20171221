﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.OleDb;
using WH_TalentSoft.Models;

namespace WH_TalentSoft.Controllers
{
    public class SessionPrController : Controller
    {
        private Entities db = new Entities();
        // GET: SessionPr

        public ActionResult Index(string p1, string p2, string p3,string p4,string p5)
        { 
            return Json(db.ValidationSousSession(p1,p2,p3,p4,p5),JsonRequestBehavior.AllowGet);
        }
        


        public ActionResult Stagiaires(string p1, string p2, string p3, string p4,string p5,string p6)
        {
            return Json(db.ValidationStagiaire(p1,p2,p3,p4,p5,p6), JsonRequestBehavior.AllowGet);
        }


        //public ActionResult Index()
        //{
        //    //var Formation = from s in db.ValidationSousSession
        //    //                select s;

        //    return View();
        //}

        // GET: SessionPr/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SessionPr/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SessionPr/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SessionPr/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SessionPr/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SessionPr/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SessionPr/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
